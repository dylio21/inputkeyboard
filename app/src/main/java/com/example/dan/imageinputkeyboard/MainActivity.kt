package com.example.dan.imageinputkeyboard

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iv_text_mode.setOnClickListener { setTextInputType()
            showSoftKeyboard(et_message) }
        iv_image_mode.setOnClickListener { setImageInputType() }
        rv_images.adapter = ImagesAdapter(100)
        rv_images.layoutManager = GridLayoutManager(this, 3)
    }

    fun setTextInputType(){
        rv_images.visibility=View.GONE
        iv_text_mode.setImageResource(R.drawable.ic_keyboard_on)
        iv_image_mode.setImageResource(R.drawable.ic_camera_off)
    }

    fun setImageInputType(){
        hideSoftKeyboard(et_message)
        iv_text_mode.setImageResource(R.drawable.ic_keyboard_off)
        iv_image_mode.setImageResource(R.drawable.ic_camera_on)
        rv_images.visibility=View.VISIBLE
    }

    fun hideSoftKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

}