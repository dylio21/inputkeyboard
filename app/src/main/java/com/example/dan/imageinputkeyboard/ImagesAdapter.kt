package com.example.dan.imageinputkeyboard

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class ImagesAdapter(val items:Int): RecyclerView.Adapter<ImagesViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ImagesViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.listitem_image_attachment,p0,false)
        return ImagesViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items
    }

    override fun onBindViewHolder(p0: ImagesViewHolder, p1: Int) {
        p0.bind()
    }
}

class ImagesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun bind(){

    }

}
