package com.example.dan.imageinputkeyboard

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View

class KeyboardRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int) : super(context, attributeSet, defStyle)

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        val heightSpec = View.MeasureSpec.makeMeasureSpec(360, View.MeasureSpec.AT_MOST)
        super.onMeasure(widthSpec, heightSpec)
    }
}